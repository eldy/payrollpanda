import React, { Component } from 'react'
import 'styles/icons'
import { GlobalStyle } from 'styles/global'

import Header from 'Pages/Layout/Header'
import Footer from 'Pages/Layout/Footer'
import Breadcrumb from 'Pages/Layout/Breadcrumb'
import UserRegistration from 'Pages/User/UserRegistration'

class App extends Component {
	render() {
		return (
			<div>
				<GlobalStyle />
				<Header />
				<Breadcrumb
					data={[
						{
							label: 'Users',
							path: 'user'
						},
						{
							label: 'Add User',
							path: 'user/add'
						}
					]}
				/>
				<UserRegistration />
				<Footer>footer</Footer>
			</div>
		)
	}
}

export default App
