import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faBars,
	faUser,
	faHome,
	faAngleRight,
	faPuzzlePiece,
	faFutbol,
	faRecycle,
	faBicycle,
	faLifeRing,
	faMinusSquare,
	faPlusSquare,
	faTrash
} from '@fortawesome/free-solid-svg-icons'

library.add(
	faBars,
	faUser,
	faHome,
	faAngleRight,
	faPuzzlePiece,
	faFutbol,
	faRecycle,
	faBicycle,
	faLifeRing,
	faMinusSquare,
	faPlusSquare,
	faTrash
)
