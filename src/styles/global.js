import styled, { createGlobalStyle } from 'styled-components'

export const Flex = styled.div`
	display: flex;
	align-items: ${props => props.alignItem};
`

//global styles
export const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Open+Sans');
  
  body {
    font-family: 'Open Sans', sans-serif;
    margin: 0;
  }
`

//variables
export const Color = {
	green: '#00ab0c',
	grey: '#f0f0f0',
	darkerGrey: '#999999',
	darkGrey: '#777777bf'
}

export const BorderRadius = '5px'
export const BorderRadiusMinimal = '3px'
