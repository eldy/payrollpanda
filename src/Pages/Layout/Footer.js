import React from 'react'
import styled from 'styled-components'
import { Flex } from 'styles/global'

const Container = styled.div`
	border-top: 1px solid #ddd;
	width: 100%;
	background: white;
`
const Info = styled(Flex)`
	display: flex;
	justify-content: space-between;
	padding: 20px 50px;
	color: grey;
	font-size: 13px;
`

const Footer = () => (
	<Container>
		<Info>
			<div>Dummy app</div>
			<div>Version 1.0</div>
		</Info>
	</Container>
)

export default Footer
