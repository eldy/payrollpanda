import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Flex, Color } from 'styles/global'

const Container = styled(Flex)`
	justify-content: space-between;
	padding: 15px 50px;
	border-bottom: 2px solid ${Color.green};
`

const Logo = styled.div`
	font-size: 18px;
	font-weight: bold;
	display: inline;
	margin: 0 10px;
`

const Header = () => (
	<Container>
		<div>
			<FontAwesomeIcon icon="bars" color={Color.green} />
			<Logo>Dummy App</Logo>
		</div>
		<Flex>
			<FontAwesomeIcon
				icon="user"
				color={Color.green}
				style={{ margin: '3px 8px 0 0' }}
			/>
			<div>
				<span>John</span> | Logout
			</div>
		</Flex>
	</Container>
)

export default Header
