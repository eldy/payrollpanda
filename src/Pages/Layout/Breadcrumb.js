import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Flex } from 'styles/global'

const Container = styled(Flex)`
	padding: 10px 50px;
`

const IconWrap = styled.div`
	padding: 0 12px;
	display: inline-block;
`

const Breadcrumb = ({ data }) => (
	<Container>
		<FontAwesomeIcon style={{ marginTop: '3px' }} icon="home" />
		{data.map((o, i) => (
			<div key={o.label}>
				<IconWrap>
					<FontAwesomeIcon color="#ddd" icon="angle-right" />
				</IconWrap>
				{o.label}
			</div>
		))}
	</Container>
)

export default Breadcrumb
