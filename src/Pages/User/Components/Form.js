import React, { Component } from 'react'
import { Flex, Color } from 'styles/global'
import styled from 'styled-components'
import Radio from 'Pages/Common/Radio'
import Checkbox from 'Pages/Common/Checkbox'
import Input from 'Pages/Common/Input'

const Wrap = styled(Flex)`
	justify-content: space-between;
`

const Left = styled.div`
	width: 500px;
`

const FormGroup = styled.div`
	margin-bottom: 20px;
	position: relative;

	label {
		display: block;
		text-transform: capitalize;
		margin-bottom: 8px;
		font-weight: bold;
	}
`

const FormGroupButton = styled.div`
	margin-top: 60px;
	text-align: right;
`

const Button = styled.input`
	${props =>
		props.primary
			? `background: ${Color.green}; color: white; margin-left: 10px`
			: `background: ${Color.grey}; color: #333`};
	display: inline-block;
	padding: 8px;
	border-radius: 5px;
	width: 100px;
	text-align: center;
	cursor: pointer;
	border: none;
	font-size: 16px;
	outline: none;
`

const formInitialState = {
	info: [
		{
			type: 'text',
			name: 'fullname',
			label: 'Full Name',
			required: true,
			floatMsg: 'Your full name on your ID.',
			showFloatMsg: false,
			value: ''
		},
		{
			type: 'email',
			name: 'email',
			label: 'Email',
			required: true,
			floatMsg: 'Please use company email.',
			showFloatMsg: false,
			value: ''
		},
		{
			type: 'text',
			name: 'city',
			label: 'City',
			required: false,
			value: ''
		}
	],
	frequency: [
		{
			name: 'always',
			label: 'Always',
			value: true
		},
		{
			name: 'sometimes',
			label: 'Sometimes',
			value: false
		},
		{
			name: 'never',
			label: 'Never',
			value: false
		}
	],
	days: [
		{ name: 'sun', label: 'Sun', value: false },
		{ name: 'mon', label: 'Mon', value: false },
		{ name: 'tue', label: 'Tue', value: false },
		{ name: 'wed', label: 'Wed', value: false },
		{ name: 'thu', label: 'Thu', value: false },
		{ name: 'fri', label: 'Fri', value: false },
		{ name: 'sat', label: 'Sat', value: false }
	]
}

export default class Form extends Component {
	state = formInitialState
	componentDidUpdate(prevProps) {
		if (
			prevProps.submitting &&
			prevProps.submitting !== this.props.submitting
		) {
			this.resetForm()
		}
	}
	handleFocus = e => {
		const { info } = this.state
		this.setState({
			info: info.map(o =>
				o.name === e.target.name ? { ...o, showFloatMsg: true } : o
			)
		})
	}
	handleInputTextChange = e => {
		const { info } = this.state
		this.setState({
			info: info.map(o =>
				o.name === e.target.name ? { ...o, value: e.target.value } : o
			)
		})
	}
	handleInputRadioChange = e => {
		const { frequency } = this.state
		this.setState({
			frequency: frequency.map(o =>
				o.name === e.target.id ? { ...o, value: true } : { ...o, value: false }
			)
		})
	}
	handleInputCheckboxChange = e => {
		const { days } = this.state
		this.setState({
			days: days.map(o =>
				o.name === e.target.name ? { ...o, value: !o.value } : o
			)
		})
	}
	handleSubmit = e => {
		e.preventDefault()
		const { info, frequency, days } = this.state

		const [fullname, email, city] = info.map(o => o.value)
		this.props.submitForm({
			fullname,
			email,
			city,
			frequency: frequency.filter(o => o.value).map(o => o.name),
			days: days.filter(o => o.value)
		})
	}
	resetForm = e => {
		this.setState(formInitialState)
	}
	render() {
		const { info, frequency, days } = this.state
		const { submitting } = this.props
		return (
			<form onSubmit={this.handleSubmit}>
				<Wrap>
					<Left>
						{info.map(obj => (
							<FormGroup key={obj.name}>
								<Input
									type={obj.type}
									name={obj.name}
									label={obj.label}
									required={obj.required}
									floatMsg={obj.floatMsg}
									showFloatMsg={obj.showFloatMsg}
									value={obj.value}
									handleFocus={this.handleFocus}
									handleInputChange={this.handleInputTextChange}
								/>
							</FormGroup>
						))}
					</Left>
					<div>
						<FormGroup>
							<label>Ride in group?</label>
							<Flex>
								{frequency.map(obj => (
									<Flex key={obj.name}>
										<Radio
											id={obj.name}
											name="frequency"
											label={obj.label}
											value={obj.value}
											handleChange={this.handleInputRadioChange}
										/>
									</Flex>
								))}
							</Flex>
						</FormGroup>
						<FormGroup>
							<label>Days of the week</label>
							<Flex>
								{days.map(obj => (
									<Flex key={obj.name}>
										<Checkbox
											id={obj.name}
											name={obj.name}
											label={obj.label}
											value={obj.value}
											handleChange={this.handleInputCheckboxChange}
										/>
									</Flex>
								))}
							</Flex>
						</FormGroup>

						<FormGroupButton>
							<Button onClick={this.resetForm} type="button" value="Cancel" />
							<Button
								primary
								disabled={submitting}
								type="submit"
								value={submitting ? 'Saving...' : 'Save'}
							/>
						</FormGroupButton>
					</div>
				</Wrap>
			</form>
		)
	}
}
