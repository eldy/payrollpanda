import React, { Component } from 'react'
import styled from 'styled-components'
import { Flex, Color, BorderRadius } from 'styles/global'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Container = styled.div`
	padding: 40px 0 20px 0;
`
const Head = styled(Flex)`
	justify-content: space-between;
	background: ${Color.grey};
	padding: 12px 16px;
	border-top-left-radius: ${BorderRadius};
	border-top-right-radius: ${BorderRadius};
	border-bottom-left-radius: ${prop =>
		prop.borderRadiusBottom ? BorderRadius : 0};
	border-bottom-right-radius: ${prop =>
		prop.borderRadiusBottom ? BorderRadius : 0};
`
const Title = styled.div`
	font-weight: bold;
`
const Close = styled.div`
	cursor: pointer;
	span {
		margin-right: 8px;
		user-select: none;
	}
	svg {
		color: ${Color.darkerGrey};
	}
`
const Content = styled.div`
	padding: 12px;
	border: 1px solid ${Color.grey};
	border-top: none;
	border-bottom-left-radius: ${BorderRadius};
	border-bottom-right-radius: ${BorderRadius};
	clear: both;

	svg {
		float: left;
		padding: 12px;
	}
	p {
		display: inline;
	}
`

class Help extends Component {
	state = {
		infoOpen: false
	}

	toggleInfo = e => this.setState({ infoOpen: !this.state.infoOpen })

	render() {
		const { infoOpen } = this.state
		return (
			<Container>
				<div>
					<Head borderRadiusBottom={!infoOpen}>
						<Title>Help</Title>
						<Close onClick={this.toggleInfo}>
							<span>{infoOpen ? 'close' : 'open'}</span>
							<FontAwesomeIcon
								icon={infoOpen ? 'minus-square' : 'plus-square'}
							/>
						</Close>
					</Head>
					{infoOpen && (
						<Content>
							<FontAwesomeIcon
								size="5x"
								color={Color.darkerGrey}
								icon="life-ring"
							/>
							<p>
								Lorem ipsum dolor sit amet, ius an deserunt complectitur, causae
								cetero ea pri. Dolorum eleifend eloquentiam sit te. Sea ei case
								scripserit persequeris, eu liber forensibus nec. Nec ei fastidii
								maluisset, ut postulant vituperata vix. Omnium maluisset ut per.
								Nemore oportere maiestatis duo ex, pro vocent disputationi no,
								at vim quas disputando. Cum omnis idque ignota ut, ponderum
								appellantur consectetuer et cum. Sea ea aeque invenire. Te sea
								adhuc error, unum intellegat scribentur nec ut, cum quem nullam
								adolescens et. In nam omittam erroribus, no quidam eruditi sed.
								Tritani sententiae adversarium in duo, te homero accusam per.
								Lorem ipsum dolor sit amet, ius an deserunt complectitur, causae
								cetero ea pri. Dolorum eleifend eloquentiam sit te. Sea ei case
								scripserit persequeris, eu liber forensibus nec. Nec ei fastidii
								maluisset, ut postulant vituperata vix. Omnium maluisset ut per.
								Nemore oportere maiestatis duo ex, pro vocent disputationi no,
								at vim quas disputando. Cum omnis idque ignota ut, ponderum
								appellantur consectetuer et cum. Sea ea aeque invenire. Te sea
								adhuc error, unum intellegat scribentur nec ut, cum quem nullam
								adolescens et. In nam omittam erroribus, no quidam eruditi sed.
								Tritani sententiae adversarium in duo, te homero accusam per.
							</p>
						</Content>
					)}
				</div>
			</Container>
		)
	}
}

export default Help
