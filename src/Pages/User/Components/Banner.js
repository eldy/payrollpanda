import React from 'react'
import styled from 'styled-components'
import { Color } from 'styles/global'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Flex = styled.div`
	display: flex;
	align-items: center;
	padding: 0 60px 0 0;
	padding-right: ${props => props.noPaddingRight && '0'};

	svg {
		margin: 0 10px 0 5px;
		width: 40px !important;
	}
`

const Container = styled.div`
	display: flex;
	background: ${Color.grey};
	justify-content: space-between;
	padding: 40px 80px;
`

const Label = styled.div`
	color: ${Color.darkerGrey};
	font-size: 13px;
`
const Text = styled.div`
	color: ${props => (props.black ? '#333' : Color.green)};
	font-weight: bold;
	font-size: 16px;
`
const Banner = () => (
	<Container>
		<Flex>
			<Flex>
				<FontAwesomeIcon
					size="3x"
					color={Color.darkerGrey}
					icon="puzzle-piece"
				/>
				<div>
					<Label>Sector</Label>
					<Text black>Sport</Text>
				</div>
			</Flex>

			<Flex>
				<FontAwesomeIcon size="3x" color={Color.darkerGrey} icon="futbol" />
				<div>
					<Label>Sport Type</Label>
					<Text black>Football</Text>
				</div>
			</Flex>

			<Flex>
				<FontAwesomeIcon size="3x" color={Color.darkerGrey} icon="bicycle" />
				<div>
					<Label>Mode</Label>
					<Text black>Mount Cross</Text>
				</div>
			</Flex>
		</Flex>

		<Flex noPaddingRight>
			<FontAwesomeIcon size="3x" color={Color.darkerGrey} icon="recycle" />
			<div>
				<Label>Responsibility</Label>
				<Text>Be Green!</Text>
			</div>
		</Flex>
	</Container>
)

export default Banner
