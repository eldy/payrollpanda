import React from 'react'
import styled from 'styled-components'
import { Color } from 'styles/global'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Table = styled.table`
	width: 100%;
	border-collapse: collapse;
	border: none;

	thead {
		th {
			text-align: left;
			padding: 12px;
			color: white;
		}
		background: ${Color.green};
	}
	tbody {
		tr {
			&:nth-child(even) {
				background: #f2f2f2;
			}
			&:hover {
				background: #00ab0c61;

				td {
					&:last-child {
						svg {
							visibility: visible;
						}
					}
				}
			}
			td {
				text-transform: capitalize;
				padding: 8px;
				svg {
					cursor: pointer;
					padding: 5px;
					color: ${Color.darkGrey};
				}
				&:last-child {
					svg {
						visibility: hidden;
					}
				}
			}
		}
	}
`
const NoUser = styled.tr`
	text-align: center;
	td {
		padding: 10px;
		&:hover {
			background: white;
		}
	}
`
const UserList = ({ users = [], deleteUser }) => (
	<Table>
		<thead>
			<tr>
				<th>Full Name</th>
				<th>Email</th>
				<th>City</th>
				<th>Ride in group</th>
				<th>Days of the week</th>
				<th>Registration day</th>
				<th />
			</tr>
		</thead>
		<tbody>
			{users.length === 0 && (
				<NoUser>
					<td colSpan="7">No user</td>
				</NoUser>
			)}
			{users.map(o => (
				<tr key={o.id}>
					<td>{o.fullname}</td>
					<td>{o.email}</td>
					<td>{o.city}</td>
					<td>{o.days}</td>
					<td>{o.frequency}</td>
					<td>{o.registrationDate}</td>
					<td>
						<FontAwesomeIcon icon="trash" onClick={deleteUser(o.id)} />
					</td>
				</tr>
			))}
		</tbody>
	</Table>
)

export default UserList
