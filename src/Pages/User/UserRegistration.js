import React, { Component } from 'react'
import Banner from 'Pages/User/Components/Banner'
import Help from 'Pages/User/Components/Help'
import Form from 'Pages/User/Components/Form'
import UserList from 'Pages/User/Components/UserList'
import moment from 'moment'
import faker from 'faker'
import daysFormatter from 'Utils/daysFormatter'
import delay from 'delay'

import styled from 'styled-components'
const InnerWrap = styled.div`
	padding: 0 80px 40px 80px;
`

class UserRegistration extends Component {
	state = {
		users: []
	}
	componentDidMount() {
		//genenrate seed data randomly
		const daysDefault = ['always', 'sometimes', 'never']
		const users = [...Array(3)].map((_, i) => ({
			id: faker.random.uuid(),
			fullname: faker.name.findName(),
			email: faker.internet.email(),
			city: faker.address.city(),
			frequency: [faker.date.weekday(), faker.date.weekday()].join(', '),
			days: daysDefault[Math.floor(Math.random() * daysDefault.length)],
			registrationDate: moment(faker.date.past()).format('D/M/YYYY - h:mmA')
		}))

		this.setState({
			users
		})
	}

	submitForm = ({ fullname, email, city, frequency, days }) => {
		//payload to be send to the backend
		const payload = {
			fullname,
			email,
			city,
			frequency,
			days: days.map(o => o.name).join(', '), //assuming backend don't need the label property, so just store in format of 'sun,mon,wed'..
			registrationDate: new Date()
		}

		this.setState(
			{
				submittingForm: true
			},
			async () => {
				await delay(500) // faking a request to backend

				const { users } = this.state

				//then just assume we got this response from backend
				const response = {
					...payload,
					days: daysFormatter(days).join(', '),
					id: faker.random.uuid(),
					registrationDate: moment(payload.registrationDate).format(
						'D/M/YYYY - h:mmA'
					)
				}

				//then we append the newly insert data to a new row
				this.setState({
					submittingForm: false,
					users: [...users, response]
				})
			}
		)
	}

	deleteUser = id => e => {
		const { users } = this.state
		this.setState({
			users: users.filter(o => o.id !== id)
		})
	}

	render() {
		const { users, submittingForm } = this.state
		return (
			<div>
				<Banner />
				<InnerWrap>
					<Help />
					<h2>User Registration</h2>
					<Form submitForm={this.submitForm} submitting={submittingForm} />
					<h2>Bikers</h2>
					<UserList users={users} deleteUser={this.deleteUser} />
				</InnerWrap>
			</div>
		)
	}
}

export default UserRegistration
