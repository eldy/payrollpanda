import React from 'react'
import styled from 'styled-components'
import { Color } from 'styles/global'

export const Input = styled.input`
	& + label {
		text-transform: capitalize !important;
		font-weight: normal !important;
	}
	&:checked,
	&:not(:checked) {
		position: absolute;
		left: -9999px;
	}
	&:checked + label,
	&:not(:checked) + label {
		position: relative;
		padding-left: 28px;
		padding-right: 30px;
		cursor: pointer;
		line-height: 22px;
		display: inline-block;
		font-size: 14px;
		color: #333;
	}
	&:checked + label:before,
	&:not(:checked) + label:before {
		content: '';
		position: absolute;
		left: 0;
		top: 0;
		width: 18px;
		height: 18px;
		border: 2px solid ${Color.darkGrey};
		border-radius: ${props => (props.type === 'radio' ? '100%' : '3px')};
		background: #fff;
	}
	&:checked + label {
		color: ${Color.green};
	}
	&:checked + label:before {
		border: 2px solid ${Color.green};
	}
	&:hover {
		& + label {
			color: ${Color.green};
		}
		& + label:before {
			border-color: ${Color.green};
		}
	}
	&:checked + label:after,
	&:not(:checked) + label:after {
		color: ${Color.green};
		content: '';
		width: 10px;
		height: 10px;
		background: ${props => props.type === 'radio' && '#333'};
		${props =>
			props.type === 'checkbox'
				? `content: url(
						"data:image/svg+xml;charset=UTF-8,<svg xmlns='http://www.w3.org/2000/svg' width='27' height='28' viewBox='0 0 60 60'><path d='M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z'/></svg>"
				  )`
				: `content:''`};
		position: absolute;
		top: 6px;
		left: 6px;
		border-radius: ${props => (props.type === 'radio' ? '100%' : '3px')};
	}
	&:not(:checked) + label:after {
		opacity: 0;
	}
	&:checked + label:after {
		transition: 0.25s opacity ease;
		opacity: 1;
	}
`

const Radio = ({ id, name, label, value, handleChange }) => (
	<>
		<Input
			type="radio"
			id={id}
			name={name}
			checked={value}
			onChange={handleChange}
		/>
		<label htmlFor={id}>{label}</label>
	</>
)

export default Radio
