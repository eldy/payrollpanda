import React from 'react'
import styled from 'styled-components'
import { Color, BorderRadiusMinimal, Flex } from 'styles/global'

const Input = styled.div`
	input {
		padding: 8px;
		font-size: 16px;
		outline: none;
		border: 2px solid ${Color.darkGrey};
		border-radius: ${BorderRadiusMinimal};
		width: 100%;
		box-sizing: border-box;

		:focus {
			border-width: 2px;
			border-color: ${Color.green};
		}
	}

	span {
		position: absolute;
		right: 0;
		font-size: 13px;
		top: 5px;
		color: ${Color.darkGrey};
	}
`

const Asterisk = styled.div`
	color: red;
	margin-left: 5px;
`

const FloatMsg = styled.div`
	font-size: 13px;
	margin-top: 5px;
	color: ${Color.darkGrey};
`

const InputElem = ({
	type,
	name,
	label,
	required,
	floatMsg,
	showFloatMsg,
	value,
	handleFocus,
	handleInputChange
}) => (
	<Input>
		{!required && <span>Optional</span>}

		<Flex>
			<label htmlFor={name}>{label}</label>
			{required && <Asterisk>*</Asterisk>}
		</Flex>

		<input
			id={name}
			type={type}
			name={name}
			value={value}
			required={required}
			onFocus={handleFocus}
			onChange={handleInputChange}
		/>

		{showFloatMsg && <FloatMsg>{floatMsg}</FloatMsg>}
	</Input>
)

export default InputElem
