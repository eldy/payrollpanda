import React from 'react'
import { Input } from './Radio' //checkbox use style from radio

const Checkbox = ({ id, name, label, value, handleChange }) => (
	<>
		<Input
			onChange={handleChange}
			type="checkbox"
			id={id}
			name={name}
			checked={value}
		/>
		<label htmlFor={id}>{label}</label>
	</>
)
export default Checkbox
