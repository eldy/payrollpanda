import daysFormatter from './daysFormatter'

describe('test daysFormatter', () => {
	test('it should return ["Sun"]', () => {
		expect(daysFormatter([{ name: 'sun', label: 'Sun' }])).toEqual(['Sun'])
	})
	test('it should return ["Weekends"]', () => {
		expect(
			daysFormatter([
				{ name: 'sun', label: 'Sun' },
				{ name: 'sat', label: 'Sat' }
			])
		).toEqual(['Weekends'])
	})
	test('it should return ["Week days"]', () => {
		expect(
			daysFormatter([
				{ name: 'mon', label: 'Mon' },
				{ name: 'tue', label: 'Tue' },
				{ name: 'wed', label: 'Wed' },
				{ name: 'thu', label: 'Thu' },
				{ name: 'fri', label: 'Fri' }
			])
		).toEqual(['Week days'])
	})
	test('it should return ["Weekends", "Mon", "Wed"]', () => {
		expect(
			daysFormatter([
				{ name: 'sun', label: 'Sun' },
				{ name: 'sat', label: 'Sat' },
				{ name: 'mon', label: 'Mon' },
				{ name: 'wed', label: 'Wed' }
			])
		).toEqual(['Weekends', 'Mon', 'Wed'])
	})

	test('it should return ["Week days", "Sat"]', () => {
		expect(
			daysFormatter([
				{ name: 'mon', label: 'Mon' },
				{ name: 'tue', label: 'Tue' },
				{ name: 'wed', label: 'Wed' },
				{ name: 'thu', label: 'Thu' },
				{ name: 'fri', label: 'Fri' },
				{ name: 'sat', label: 'Sat' }
			])
		).toEqual(['Week days', 'Sat'])
	})
})
