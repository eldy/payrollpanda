//maybe there's other better alternative instead of doing bunch of includes because each includes means one iteration,
//but in this case, the days will most likely just be 7 in length, so I think it's not a big deal

export default days => {
	const daysByName = days.map(o => o.name)

	if (days.length === 7) {
		return ['Everyday']
	}

	const isWeekends = daysByName.includes('sun') && daysByName.includes('sat')
	const isWeekdays =
		daysByName.includes('mon') &&
		daysByName.includes('tue') &&
		daysByName.includes('wed') &&
		daysByName.includes('thu') &&
		daysByName.includes('fri')

	if (isWeekends) {
		return [
			'Weekends',
			...days
				.filter(o => o.name !== 'sun' && o.name !== 'sat')
				.map(o => o.label)
		]
	}

	if (isWeekdays) {
		return [
			'Week days',
			...days
				.filter(
					o =>
						o.name !== 'mon' &&
						o.name !== 'tue' &&
						o.name !== 'wed' &&
						o.name !== 'thu' &&
						o.name !== 'fri'
				)
				.map(o => o.label)
		]
	}

	return [...days.map(o => o.label)]
}
